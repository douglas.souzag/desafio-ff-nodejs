[![Nodejs](https://img.shields.io/badge/nodejs-v12.18.1-<COLOR>.svg)](https://shields.io/)
[![Express](https://img.shields.io/badge/express-v4.17.1-green.svg)](https://shields.io/)
[![Knex](https://img.shields.io/badge/knex-v0.21.1-blue.svg)](https://shields.io/)

[![Axios](https://img.shields.io/badge/axios-v0.19.2-red.svg)](https://shields.io/)
[![Vue](https://img.shields.io/badge/vue-v2.6.11-blue.svg)](https://shields.io/)
[![Vuetify](https://img.shields.io/badge/vuetify-v2.2.11-brown.svg)](https://shields.io/)


# [Landing Page para divulgação de uma Smart TV Samsung]

Este pequeno projeto foi desenvolvido como desafio de entrevista para a empresa [Flip Flop Lab.](https://www.flipfloplab.com.br/)             


## Rodando Servidor

```
cd .\server\
npm install
npm start
```
### Configurações de porta em .\server\src\server.js, por padrão é 3333
### O banco de dados está hospedado em um serviço gratuito online: https://www.elephantsql.com, portanto não há necessidade de criar localmente.

## Rodando Cliente

```
cd .\client\
npm install
npm run serve
```
Coloque a mesma porta do servidor em .\client\src\services\config.js, por padrão é 3333