const express = require('express')
const routes = express.Router()

const EmailController = require('./controllers/EmailController')


//Email
routes.get('/emails',EmailController.index)
routes.post('/emails',EmailController.create)
routes.delete('/emails',EmailController.delete)

module.exports = routes