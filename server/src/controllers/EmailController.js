const knex = require('../database')
const emailValidacaoRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

module.exports = {
	async index(req, res) { 
		const results = await knex('emails')
		return res.json(results)
	},
	async create(req, res, next) {
		try {
			const { email } = req.body

			if (!emailValidacaoRegex.test(email)){
				return res.status(202).send({message:"Email inválido"})
			}

			const preQuery = knex('emails')
			.where({email})

			const results = await preQuery

			if(results.length > 0){
				return res.status(200).send({message:"Email já está no banco de dados"})
			}

			await knex('emails').insert({
				email
			})

			return res.status(201).send({message:"Email adicionado com sucesso!"})
		} catch (error) {
			next(error)
		}
	},
	async delete(req, res, next) {
			try {
				const { id } = req.query
				await knex('emails')
				.where({ id })
				.del()

				return res.status(200).send({message:"Email removido com sucesso!"})
			} catch (error) {
				next(error)
			}
	}
}