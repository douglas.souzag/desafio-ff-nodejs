exports.seed = function(knex) {
  return knex('emails').del().then(function () {
    return knex('emails').insert([
      {
        email: 'email.desafio1@gmail.com',
      },
      {
        email: 'email.desafio2@gmail.com',
      },
      {
        email: 'email.desafio3@gmail.com'
      }
    ]);
  });
};
