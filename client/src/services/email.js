import { http } from './config'

export default{
	listar:() => {
		return http.get('emails')
	},
	criar:(email) => {

		
		const data = {
			email:email
		}
		console.log(data)
		return http.post('emails',data)
	},
	deletar:(id) => {
		const params = {
			id:id
		}
		return http.delete('emails',{params})
	},
}
