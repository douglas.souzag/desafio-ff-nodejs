//Services
import Email from '../../services/email'

export default {
    components: {
    },
    data () {
      return {
        arrayEmails: null,
        alerta: {
					cor: 'green',
					visivel: false,
					mensagem: null
				}
      }
    },
    mounted () {
        Email.listar().then(resposta =>{
				this.arrayEmails = resposta.data;
      })
    },
    methods: {
      removerEmail(id){
				Email.deletar(id).then(resposta =>{
					var removeIndex = this.arrayEmails.findIndex(e => e.id === id)
					this.arrayEmails.splice(removeIndex, 1);
					console.log(resposta)
					this.alerta.mensagem = resposta.data.message
					this.alerta.visivel = true
        })
      }
    }
  }
