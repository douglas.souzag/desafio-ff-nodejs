//Services
import Email from '../../services/email'

export default {
    components: {
    },
    data () {
      return {
        email: null,
				alerta: {
					cor: 'green',
					visivel: false,
					mensagem: null
				}
      }
    },
    methods: {
			inserirEmail(){
				console.log(this.email)
        Email.criar(this.email).then(resposta =>{
					console.log(resposta)
					
					this.alerta.cor = 'green'

					if(resposta.status===200){
						this.alerta.cor = 'orange'
					}
					if(resposta.status===202){
						this.alerta.cor = 'red'
					}
					
					this.alerta.mensagem = resposta.data.message
					this.alerta.visivel = true 
					console.log(this.alerta)
        })
				this.email = null
      }
    }
  }
