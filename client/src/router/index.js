import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home/Home.vue'
import Painel from '../views/Painel/Painel.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '*',
    redirect: '/home',
  },
  {
    path: '/home',
    name: 'Home',
    component: Home
  },
  {
    path: '/painel',
    name: 'Painel',
    component: Painel
  },
]

const router = new VueRouter({
  routes
})

export default router
